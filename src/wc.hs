module Main where
import Options.Applicative
import qualified Data.Text.Lazy.IO as Txt.IO
import qualified Data.Text.Lazy as Txt
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as BS (lines,words)

class StringProps s where
    strlen :: s -> Integer
    strlines :: s -> [s]
    strwords :: s -> [s]

instance StringProps Txt.Text where
    strlen = toInteger . Txt.length
    strlines = Txt.lines
    strwords = Txt.words

instance StringProps BS.ByteString where
    strlen = toInteger . BS.length
    strlines = BS.lines
    strwords = BS.words

main :: IO ()
main = do
    opts <- getOpts 
    let flgs = optFlags opts
    let arg = optArgs opts
    bsfile <- BS.readFile arg
    txtfile <- Txt.IO.readFile arg

    printLine arg $ getAll flgs txtfile bsfile

getAll :: Flags -> Txt.Text -> BS.ByteString -> [Integer]
getAll f txt bs = if null flagList then def else flagList where
    flagList = l ++ w ++ m ++ c ++ l_
    c = [getChars bs | flagc f]
    m = [getChars txt | flagm f]
    l = [getLines txt | flagl f]
    l_ = [getMaxLine txt | flagL f]
    w = [getWords txt | flagw f]
    def = [getLines txt, getWords txt, getChars bs]

getChars :: StringProps s => s -> Integer
getChars = strlen 

getLines :: StringProps s => s -> Integer
getLines = toInteger . length . strlines

getMaxLine :: StringProps s => s -> Integer
getMaxLine = maximum . map strlen . strlines

getWords :: StringProps s => s -> Integer
getWords = toInteger . length . strwords

printLine :: FilePath -> [Integer] -> IO()
printLine fp i = putStrLn $ unwords (map show i) ++ " " ++ fp

data Flags = Flags {
    flagc :: Bool,
    flagm :: Bool,
    flagl :: Bool,
    flagL :: Bool,
    flagw :: Bool
}

data Opts = Opts {
    optFlags :: Flags,
    optArgs :: String
}

getOpts :: IO Opts
getOpts = execParser $ info (helper <*> (Opts <$> flags <*> args))
    (fullDesc
    <> header "wc - print newline, word and byte counts for each file")

flags :: Parser Flags
flags = Flags <$> byte <*> char <*> line <*> maxLine <*> word

byte :: Parser Bool
byte = switch (short 'c' <> long "bytes")

char :: Parser Bool
char = switch (short 'm' <> long "chars")

line :: Parser Bool
line = switch (short 'l' <> long "lines")

maxLine :: Parser Bool
maxLine = switch (short 'L' <> long "max-line-length")

word :: Parser Bool
word = switch (short 'w' <> long "words")

args :: Parser String
args = argument str (metavar "FILE")
