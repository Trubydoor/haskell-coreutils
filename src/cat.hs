module Main where
import Options.Applicative
import qualified Data.ByteString.Lazy as BS

main :: IO ()
main = getOpts >>= printArg

printArg :: [String] -> IO ()
printArg [] = printArg ["-"]
printArg list = mapM_ printFile list where
        printFile file = case file of
            "-" -> BS.getContents >>= BS.putStr
            _ -> BS.readFile file >>= BS.putStr

getOpts :: IO [String]
getOpts = execParser $ info (helper <*> args)
    (fullDesc
    <> header "cat - conatenate files and print on the standard output"
    <> progDesc "Concatenate FILE(s), or standard input, to standard output"
    )

args :: Parser [String]
args = many $ argument str (metavar "FILES")
