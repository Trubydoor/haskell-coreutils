module Main where
import Options.Applicative

main :: IO ()
main = getOpts >>= printArg

printArg :: String -> IO ()
printArg "" = printArg "y"
printArg arg = putStrLn arg >>
               printArg arg

getOpts :: IO String
getOpts = execParser $ info (helper <*> args)
    (fullDesc
    <> header "yes - output a string repeatedly until killed"
    <> progDesc "Repeatedly output a line with all specified STRING(s), or 'y'"
    )

args :: Parser String
args = unwords <$> many (argument str (metavar "STRING"))
