module Main where
import Options.Applicative
import Control.Monad (void)
import System.Directory (getCurrentDirectory)

main :: IO ()
main = getOpts >> getCurrentDirectory >>= putStrLn 

getOpts :: IO ()
getOpts = void $ execParser $ info helper
    (fullDesc
    <> header "pwd - Print current working directory"
    <> progDesc "Print the full filename of the current working directory"
    )
