module Main where
import System.Exit
import Options.Applicative

main :: IO ()
main = getOpts >> exitSuccess where

getOpts :: IO (a -> a)
getOpts = execParser opts where
    opts = info helper
        (fullDesc 
        <> header "true - do nothing, successfuly"
        <> progDesc "Exit with a status code indicating success")
        
