module Main where
import System.Exit
import Options.Applicative

main :: IO ()
main = getOpts >> exitFailure where

getOpts :: IO (a -> a)
getOpts = execParser opts where
    opts = info helper
        (fullDesc 
        <> header "false - do nothing, unsuccessfuly"
        <> progDesc "Exit with a status code indicating failure")
        
